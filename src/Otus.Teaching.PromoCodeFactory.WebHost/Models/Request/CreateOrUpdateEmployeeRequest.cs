﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Request
{
    public class CreateOrUpdateEmployeeRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }
    }
}
