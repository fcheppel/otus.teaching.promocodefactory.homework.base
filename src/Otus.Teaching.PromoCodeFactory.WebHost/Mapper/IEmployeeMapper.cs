﻿using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Request;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapper
{
    public interface IEmployeeMapper
    {
        Task<Employee> MapToModelAsync(CreateOrUpdateEmployeeRequest model, Employee employee = null);
    }
}
