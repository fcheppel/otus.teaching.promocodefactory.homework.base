﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Request;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapper
{
    public class EmployeeMapper : IEmployeeMapper
    {
        private readonly IRepository<Role> _roleRepository;

        public EmployeeMapper(IRepository<Role> roleRepository)
        {
            _roleRepository = roleRepository;
        }
        public async Task<Employee> MapToModelAsync(CreateOrUpdateEmployeeRequest model, Employee employee = null)
        {
            if (employee == null)
            {
                employee = new Employee
                {
                    Id = Guid.NewGuid()
                };
            }

            var roles = await _roleRepository.GetByCondition(x => model.Roles.Contains(x.Name)) as List<Role>;

            employee.FirstName = model.FirstName;
            employee.LastName = model.LastName;
            employee.Email = model.Email;
            employee.AppliedPromocodesCount = model.AppliedPromocodesCount;
            employee.Roles = roles;

            return employee;
        }
    }
}
