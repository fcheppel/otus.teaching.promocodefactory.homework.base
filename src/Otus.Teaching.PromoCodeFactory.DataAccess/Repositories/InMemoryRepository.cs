﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateAsync(T entity)
        {
            var data = Data as List<T>;
            data.Add(entity);
            Data = data;
            entity.Id = Guid.NewGuid();

            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {
            var data = Data as List<T>;
            T rec = data.SingleOrDefault(f => f.Id == entity.Id);
            if (rec != null)
                data.Remove(rec);
            data.Add(entity);
            Data = data;

            return Task.CompletedTask;
        }

        public Task DeleteAsync(T entity)
        {
            var data = Data as List<T>;
            T rec = data.SingleOrDefault(f => f.Id == entity.Id);
            if (rec != null)
                data.Remove(rec);

            return Task.CompletedTask;
        }

        public Task<IEnumerable<T>> GetByCondition(Func<T, bool> predicate)
        {
            return Task.FromResult((Data as List<T>).Where(predicate).AsEnumerable());
        }
    }
}